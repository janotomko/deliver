module gitlab.com/jano.tomko/deliver

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/matrix-org/gomatrix v0.0.0-20190528120928-7df988a63f26
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
)
