// SPDX-License-Identifier: GPL-3.0-or-later
//
// (c) 2019 Red Hat

package parse

import (
	"bufio"
	"errors"
	"os"
	"regexp"
)

func ParseCode() (string, error) {
	var scanner = bufio.NewScanner(os.Stdin)
	var sixDigits = regexp.MustCompile(`>([0-9]{6})<`)

	for scanner.Scan() {
		var text = scanner.Text()

		if sixDigits.MatchString(text) {
			return string(sixDigits.FindSubmatch([]byte(text))[1]), nil
		}
	}
	return "", errors.New("No code found")
}
