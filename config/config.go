// SPDX-License-Identifier: GPL-3.0-or-later
//
// (c) 2019 Red Hat

package config

import (
	"github.com/BurntSushi/toml"
	"os"
)

type Account struct {
	Server      string
	Username    string
	AccessToken string `toml:"access_token"`
}

type Destination struct {
	RoomID string `toml:"room_id"`
}

type Config struct {
	Account     Account
	Destination Destination
}

func LoadConfig() (Config, error) {
	var config Config
	var prefix string
	var path string

	prefix = os.Getenv("XDG_CONFIG_HOME")
	if prefix == "" {
		prefix = os.Getenv("HOME") + "/.config"
	}

	path = prefix + "/deliver"

	_, err := toml.DecodeFile(path, &config)
	if err != nil {
		return config, err
	}
	return config, nil
}
