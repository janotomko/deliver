deliver
=======

A silly program that parses a six-digit number and sends it
as a QR code to a configured Matrix room that would better
work as a Perl script than a 7 meg statically-linked binary.

Suggested-by: Andrea Bolognani <abologna@redhat.com>

Usage
-----
Create a config file named `deliver` in your `XDG_CONFIG_HOME`:
```
[account]
server = "https://matrix.org"
username = "@joe:matrix.org"
access_token = "threelinesofrandomrunes"

[destination]
room_id = "!notsomanyrandomrunes:matrix.org"
```

To easily obtain the access token, just use curl:
```
curl -XPOST -d \
        '{"type":"m.login.password", "user":"joe", "password":"hunter2"}' \
        "https://matrix.org/_matrix/client/r0/login"
```
Don't forget to replace matrix.org with the address of your server.

Invocation:
```
$ echo '>123456<' | ~/go/deliver/deliver
```

or via mutt:

```
macro index,pager M "|~/go/deliver/deliver\n" 'send a six-digit QR code to Matrix'
```
