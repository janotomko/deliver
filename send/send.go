// SPDX-License-Identifier: GPL-3.0-or-later
//
// (c) 2019 Red Hat

package send

import (
	"bytes"
	"github.com/matrix-org/gomatrix"
	"gitlab.com/jano.tomko/deliver/config"
)

func SendPNG(config config.Config, image []byte) error {
	var resp *gomatrix.RespMediaUpload

	cli, err := gomatrix.NewClient(config.Account.Server,
		config.Account.Username,
		config.Account.AccessToken)
	if err != nil {
		return err
	}

	r := bytes.NewReader(image)
	resp, err = cli.UploadToContentRepo(r, "image/png", int64(r.Len()))
	if err != nil {
		return err
	}

	_, err = cli.SendImage(config.Destination.RoomID, "qr_code.png", resp.ContentURI)
	if err != nil {
		return err
	}

	return nil
}
