// SPDX-License-Identifier: GPL-3.0-or-later
//
// Deliver - send numeric QR codes to Matrix
//
// (c) 2019 Red Hat

package main

import (
	"fmt"
	"github.com/skip2/go-qrcode"
	"gitlab.com/jano.tomko/deliver/config"
	"gitlab.com/jano.tomko/deliver/parse"
	"gitlab.com/jano.tomko/deliver/send"
	"os"
)

func main() {
	var code string
	var conf config.Config
	var png []byte
	var err error

	conf, err = config.LoadConfig()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	fmt.Printf("Account login: '%s' Server: '%s'\nDestination: '%s'\n",
		conf.Account.Username,
		conf.Account.Server,
		conf.Destination.RoomID)

	code, err = parse.ParseCode()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("Found code: '%s'\n", code)

	png, err = qrcode.Encode(code, qrcode.Highest, -16)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	err = send.SendPNG(conf, png)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
